import java.util.ArrayList;

class CompoundTheNumbers
{
    public ArrayList<String> parseTheString(String str) {
        String[] substrs = str.split(",");
        ArrayList<Integer> parsed = new ArrayList<Integer>();
        ArrayList<String> compounded = new ArrayList<String>();
        for (String i : substrs)
            parsed.add(Integer.parseInt(i));
        parsed.add(0);
        for (int i = 0, j = 0; i < parsed.size()-1; i++, j++) {
            int counter = 0;
            if (parsed.get(i) != parsed.get(i+1)-1)
                compounded.add(Integer.toString(parsed.get(i)));
            else {
                compounded.add(Integer.toString(parsed.get(i)));
                while (parsed.get(i) == parsed.get(i+1)-1) {
                    i++; counter++;
                }
                if (counter > 1) {
                    compounded.add(compounded.get(j).concat("-" + parsed.get(i)));
                    compounded.remove(j);
                }
                else
                    compounded.add(Integer.toString(parsed.get(i)));
            }
        }
        return compounded;
    }

    public static void main(String[] args) {
        CompoundTheNumbers arr = new CompoundTheNumbers();
        ArrayList<String> compounded = arr.parseTheString(args[0]);
        System.out.print(args[0] + " -> ");
        for (String i : compounded)
            System.out.print(i + ",");
    }
}