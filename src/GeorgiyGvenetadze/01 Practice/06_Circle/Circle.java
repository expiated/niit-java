class Circle {

    private double Radius;
    private double Ference;
    private double Area;

    public void setRadius (double arg) {
        this.Radius = arg;
        this.Ference = 2 * Math.PI * arg;
        this.Area = Math.PI * Math.pow(this.Radius, 2);
    }

    public void setFerence (double arg) {
        this.Ference = arg;
        this.Radius = this.Ference / 2 / Math.PI;
        this.Area = Math.PI * Math.pow(this.Radius, 2);
    }

    public void setArea (double arg) {
        this.Area = arg;
        this.Radius = Math.sqrt(this.Area / Math.PI);
        this.Ference = 2 * Math.PI * this.Radius;
    }

    public double getRadius () {
        return this.Radius;
    }

    public double getFerence () {
        return this.Ference;
    }

    public double getArea () {
        return this.Area;
    }

    public static void main (String args[]) {
        Circle planetEarth = new Circle();
        Circle yarn = new Circle();

        planetEarth.setRadius(6378.1*1000);
        yarn.setFerence(planetEarth.getFerence());
        yarn.setFerence(yarn.getFerence() + 1);

        double diff = yarn.getRadius() - planetEarth.getRadius();
        System.out.println("The clearence between the yarn and Earth is " + diff + " meters.");

        Circle pool = new Circle();
        Circle commonArea = new Circle();
        pool.setRadius(3);
        commonArea.setRadius(pool.getRadius() + 1);

        double costOfSidewalk = 1000 * pool.getFerence();
        double costOfFence = 2000 * commonArea.getFerence();
        System.out.println("Full cost of all materials of pool is " +
                (costOfFence + costOfSidewalk) + " rubles.");
    }
}