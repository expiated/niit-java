public class Collatz {

    private static int collatzStep = 0;

    public static void main(String[] args) {
        int seq = 0, number = 0;

        for (int i = 2; i < 1000000; i++)
        {
            collatzStep = 0;
            collatzSequence(i);
            if (collatzStep > seq)
            {
                seq = collatzStep;
                number = i + 1;
            }
        }
        System.out.println("The longest sequence in range from 0 to 1000000 is "
                + seq + " and it starts from " + number);
    }

    private static void collatzSequence(long n) {
        collatzStep++;
        if (n != 1)
            if (n % 2 == 1)
                collatzSequence(3 * n + 1);
            else
                collatzSequence(n / 2);
    }
}