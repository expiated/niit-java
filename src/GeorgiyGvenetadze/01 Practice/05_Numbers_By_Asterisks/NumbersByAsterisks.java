import java.lang.reflect.Array;
import java.util.Arrays;

class NumbersByAsterisks {

    private String[] zero = {
            "  ***  ",
            " *   * ",
            "*     *",
            "*     *",
            "*     *",
            " *   * ",
            "  ***  ",
    };

    private  String[] one = {
            "   *   ",
            "  **   ",
            "   *   ",
            "   *   ",
            "   *   ",
            "   *   ",
            "  ***  ",
    };

    private  String[] two = {
            "  ***  ",
            " *   * ",
            " *  *  ",
            "   *   ",
            "  *    ",
            " *     ",
            " ***** ",
    };

    private  String[] three = {
            "  ***  ",
            " *   * ",
            "     * ",
            "    *  ",
            "     * ",
            " *   * ",
            "  ***  ",
    };

    private String[] four = {
            "   *   ",
            "  **   ",
            " * *   ",
            "*  *   ",
            "****** ",
            "   *   ",
            "   *   ",
    };

    private String[] five = {
            " ***** ",
            " *     ",
            " *     ",
            " ****  ",
            "     * ",
            " *   * ",
            "  ***  ",
    };

    private String[] six = {
            "  ***  ",
            " *   * ",
            " *     ",
            " ****  ",
            " *   * ",
            " *   * ",
            "  ***  ",
    };

    private  String[] seven = {
            " ***** ",
            "     * ",
            "    *  ",
            "   *   ",
            "  *    ",
            " *     ",
            " *     ",
    };

    private  String[] eight = {
            "  ***  ",
            " *   * ",
            " *   * ",
            "  ***  ",
            " *   * ",
            " *   * ",
            "  ***  ",
    };

    private String[] nine = {
            "  ***  ",
            " *   * ",
            " *   * ",
            "  **** ",
            "     * ",
            " *   * ",
            "  ***  ",
    };

    private String[] digitByAsterisk(char ch) {
        String[] digit = new String[7];
        switch (ch) {
            case '0' : digit = this.zero;
            break;
            case '1' : digit = this.one;
            break;
            case '2' : digit = this.two;
            break;
            case '3' : digit = this.three;
            break;
            case '4' : digit = this.four;
            break;
            case '5' : digit = this.five;
            break;
            case '6' : digit = this.six;
            break;
            case '7' : digit = this.seven;
            break;
            case '8' : digit = this.eight;
            break;
            case '9' : digit = this.nine;
            break;
        }
        return digit;
    }

    public void drawTheDigit(String str) {
        char[] arr = str.toCharArray();
        String[] temp, result = new String[7];
        for (int i = 0; i < arr.length; i++) {
            temp = Arrays.copyOf(digitByAsterisk(arr[i]), 7);
            if (i == 0) {
                result = temp;
                continue;
            }
            for (int j = 0; j < result.length; j++) {
                result[j] += temp[j];
            }
        }
        for (String i : result)
            System.out.println(i);
    }

    public static void main(String[] args) {
        NumbersByAsterisks num = new NumbersByAsterisks();
        num.drawTheDigit(args[0]);
    }
}