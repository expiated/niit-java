import java.util.ArrayList;

class ExpandTheNumbers
{
    public ArrayList<Integer> parseTheString(String str) {
        ArrayList<Integer> parsed = new ArrayList<Integer>();
        String[] substrs = str.split(",");
        for (String i : substrs) {
            if (i.contains("-")) {
                String[] contracted = i.split("-");
                int val1 = Integer.parseInt(contracted[0]);
                int val2 = Integer.parseInt(contracted[1]);
                while (val1 <= val2)
                    parsed.add(val1++);
                continue;
            }
            parsed.add(Integer.parseInt(i));
        }
        return parsed;
    }

    public static void main(String[] args) {
        ExpandTheNumbers arr = new ExpandTheNumbers();
        ArrayList<Integer> expanded = arr.parseTheString(args[0]);
        System.out.print(args[0] + " -> ");
        for (int i : expanded)
            System.out.print(i + ",");
    }
}