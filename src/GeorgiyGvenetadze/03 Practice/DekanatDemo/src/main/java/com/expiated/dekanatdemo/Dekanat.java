package com.expiated.dekanatdemo;


import java.io.*;
import java.util.ArrayList;

public class Dekanat {

    Dekanat () {
        this.students = new ArrayList<Student>();
        this.groups = new ArrayList<Group>();
    }

    private ArrayList<Student> students;
    private ArrayList<Group> groups;

    public ArrayList<Student> getStudents () {
        return this.students;
    }
    public ArrayList<Group> getGroups () {
        return this.groups;
    }

    public void createStudents (File file) throws FileNotFoundException {
        BufferedReader reader = new BufferedReader(new FileReader(file));
        this.students = new ArrayList<Student>();
        String line; int i = 0;
        try {
            while ((line = reader.readLine()) != null) {
                Student student = new Student();
                student.createStudent(++i, line);
                this.students.add(student);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void createGroups (File file) throws FileNotFoundException {
        BufferedReader reader = new BufferedReader(new FileReader(file));
        this.groups = new ArrayList<Group>();
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                Group group = new Group();
                group.createGroup(line);
                this.groups.add(group);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void distributeStudentsByGroups (ArrayList<Student> s, ArrayList<Group> g, int groupLimit) {
        int  current = 0, tail = 0;
        for (int i = 0, limit = 0; i < g.size(); i++) {
            while (limit < groupLimit && limit < (s.size() - tail)) {
                g.get(i).addStudent(s.get(current++), g.get(i));
                limit++;
            }
            limit = 0; tail += groupLimit;
        }
    }

    public void addRandomMarks (ArrayList<Student> students, int quantityOfMarks) {
        for (Student s : students)
            for (int i = 0; i < quantityOfMarks; i++)
                s.addMark((int)(Math.random()*(3+1))+2);
    }

    public void printTheStatistics (ArrayList<Group> groups) {
        for (Group g : groups) {
            System.out.println("Group: " + g.getTitle());
            System.out.println("Head: " + g.getHead().getFio());
            System.out.printf("Average mark of group: %.1f\n", g.averageMarkOfGroup());
            System.out.println("List of students:");
            for (Student s : g.getStudents())
                System.out.println(s.getId() + " " + s.getFio() + " "
                        + s.averageMark(s.getMarks()));
            System.out.println();
        }
    }

    public void transferTheStudent (Student student, Group newGroup) {
        Group temp = student.getGroup();
        if (temp.getHead().equals(student))
            initHeadElection(temp);
        temp.excludeStudent(student);
        student.enrollStudent(newGroup);
        newGroup.addStudent(student, newGroup);
    }

    public void excludeStudents (Student student) {
        Group temp = student.getGroup();
        if (temp.getHead().equals(student))
            initHeadElection(temp);
        temp.excludeStudent(student);
        this.students.remove(this.students.indexOf(student));
    }

    public void initHeadElection (Group group) {
        group.initHead(group.getStudents().get((int)
                (Math.random()*group.getStudents().size())));
    }

    public void saveChangesToFile (File file) throws FileNotFoundException {
        try {
            PrintWriter writer = new PrintWriter(file);
            for (Student s : this.students)
                writer.println(s.getFio());
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new FileNotFoundException();
        }
    }
}
