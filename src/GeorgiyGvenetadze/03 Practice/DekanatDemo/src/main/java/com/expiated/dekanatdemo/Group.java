package com.expiated.dekanatdemo;

import java.util.ArrayList;

public class Group {
    Group () {
        this.students = new ArrayList<Student>();
    }

    private String title;
    private ArrayList<Student> students;
    private Student head;

    public String getTitle () {
        return this.title;
    }

    public ArrayList<Student> getStudents () {
        return this.students;
    }

    public Student getHead () {
        return this.head;
    }

    public void createGroup (String title) {
        this.title = title;
    }

    public void addStudent (Student student, Group group) {
        this.students.add(student);
        student.enrollStudent(group);
    }

    public void initHead (Student head) {
        this.head = head;
    }

    public double averageMarkOfGroup () {
        double average = 0;
        for (Student i : this.students)
            average += i.averageMark(i.getMarks());
        return average/this.students.size();
    }

    public void excludeStudent (Student student) {
        this.students.remove(student);
    }
}
