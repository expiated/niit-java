package com.expiated.dekanatdemo;

import java.io.File;
import java.io.FileNotFoundException;

public class App
{
    final static String studentsListPath = "studentsList.txt";
    final static String groupsPath = "groups.txt";


    public static void main( String[] args ) throws FileNotFoundException {
        Dekanat d = new Dekanat();
        File file = new File (App.studentsListPath);
        //create students from file
        d.createStudents(file);
        //print identificators and names
        for (Student s : d.getStudents())
            System.out.println(s.getId() + " " + s.getFio());
        //print the students quantity
        System.out.println(d.getStudents().size());

        file = new File(App.groupsPath);
        //create groups from file
        d.createGroups(file);
        //print titles of groups
        for (Group g : d.getGroups())
            System.out.println(g.getTitle());
        //distribute students to groups with 10 students limit
        d.distributeStudentsByGroups(d.getStudents(), d.getGroups(), 10);
        //initiate the Head election in all groups
        for (Group g : d.getGroups())
            d.initHeadElection(g);
        //all students get the bunch of random mark
        d.addRandomMarks(d.getStudents(), 10);
        //print name and average mark of each student
        for (Student s : d.getStudents()){
            System.out.print(s.getFio() + " " + s.averageMark(s.getMarks()) + "\n");
            //print all marks of current student
            for (int i : s.getMarks())
                System.out.print(i + " ");
            System.out.println();
        }

        System.out.println();

        //exclude the student by name
        String nm = "Isaac Newton";
        for (int i = 0; i < d.getStudents().size(); i++)
            if (nm.equals(d.getStudents().get(i).getFio()))
                d.excludeStudents(d.getStudents().get(i));

        //exclude another one
        nm = "Euclid of Alexandria";
        for (int i = 0; i < d.getStudents().size(); i++)
            if (nm.equals(d.getStudents().get(i).getFio()))
                d.excludeStudents(d.getStudents().get(i));

        //redistribute one of students
        d.transferTheStudent(d.getStudents().get(0), d.getGroups().get(1));

        //print the statistics of each group
        d.printTheStatistics(d.getGroups());

        file = new File(App.studentsListPath);
        //save changes to the students list file
        d.saveChangesToFile(file);
    }
}
