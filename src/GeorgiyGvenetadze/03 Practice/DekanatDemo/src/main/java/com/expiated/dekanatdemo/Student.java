package com.expiated.dekanatdemo;

import java.util.ArrayList;

public class Student {
    Student () {
        this.marks = new ArrayList<Integer>();
    }

    Student (int id, String fio) {
        this.id=id;
        this.fio=fio;
        this.marks = new ArrayList<Integer>();
    }

    private int id;
    private String fio;
    private Group group;
    private ArrayList<Integer> marks;

    public int getId () {
        return this.id;
    }

    public String getFio () {
        return this.fio;
    }

    public Group getGroup () {
        return this.group;
    }

    public ArrayList<Integer> getMarks () {
        return this.marks;
    }

    public void createStudent (int id, String fio) {
        this.id = id;
        this.fio = fio;
    }

    public void enrollStudent (Group group) {
        this.group = group;
    }

    public void addMark (int mark) {
        this.marks.add(mark);
    }

    public double averageMark (ArrayList<Integer> marks) {
        double average; double temp = 0;
        for (int i : marks)
            temp += i;
        average = temp/marks.size();
        return average;
    }
}
