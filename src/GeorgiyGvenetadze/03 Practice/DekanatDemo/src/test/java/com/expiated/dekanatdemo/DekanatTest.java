package com.expiated.dekanatdemo;

import org.junit.Before;
import org.junit.Test;

import javax.jnlp.FileOpenService;
import java.io.*;
import java.util.Scanner;

import static org.junit.Assert.*;

public class DekanatTest {

    private Dekanat dekanat;
    private final String studentsPath = "studentsList.txt";
    private final String groupsPath = "groups.txt";
    private File studentFile;
    private File groupFile;

    @Before
    public void initTest() {
        this.dekanat = new Dekanat();
        this.studentFile = new File(this.studentsPath);
        this.groupFile = new File(this.groupsPath);
    }

    @Test
    public void createStudentsTest () throws Exception {
        BufferedReader reader = new BufferedReader(new FileReader(this.studentFile));
        dekanat.createStudents(this.studentFile);
        for (Student s : dekanat.getStudents())
            assertEquals(reader.readLine(), s.getFio());
    }

    @Test (expected = FileNotFoundException.class)
    public void createStudentsExceptionTest () throws Exception {
        File file = new File ("//asdsad");
        dekanat.createStudents(file);
    }

    @Test
    public void createGroupsTest () throws Exception {
        BufferedReader reader = new BufferedReader(new FileReader(this.groupFile));
        dekanat.createGroups(this.groupFile);
        for (Group g : dekanat.getGroups())
            assertEquals(reader.readLine(), g.getTitle());
    }

    @Test (expected = FileNotFoundException.class)
    public void createGroupsExceptionTest () throws Exception {
        File file = new File ("//asdsad");
        dekanat.createGroups(file);
    }

    @Test
    public void distributeStudentsByGroupsTest () throws Exception {
        dekanat.createStudents(this.studentFile);
        dekanat.createGroups(this.groupFile);
        dekanat.distributeStudentsByGroups(dekanat.getStudents(), dekanat.getGroups(), 10);
        for (Group g : dekanat.getGroups())
            for (Student s : g.getStudents())
                assertEquals(s.getGroup().getTitle(), g.getTitle());
    }

    @Test
    public void addRandomMarksTest () throws Exception {
        dekanat.createStudents(this.studentFile);
        dekanat.addRandomMarks(dekanat.getStudents(), 10);
        for (Student s : dekanat.getStudents()) {
            assertEquals(10, s.getMarks().size());
            for (int m : s.getMarks())
                assertTrue(1 < m && m < 6);
        }
    }

    @Test
    public void transferTheStudentTest () throws Exception {
        dekanat.createStudents(this.studentFile);
        dekanat.createGroups(this.groupFile);
        dekanat.distributeStudentsByGroups(dekanat.getStudents(), dekanat.getGroups(), 10);
        for (Group g : dekanat.getGroups())
            dekanat.initHeadElection(g);
        Student tempStudent = dekanat.getStudents().get(0);
        String tempGroup = tempStudent.getGroup().getTitle();
        dekanat.transferTheStudent(tempStudent, dekanat.getGroups().get(1));
        assertNotEquals(tempGroup, tempStudent.getGroup().getTitle());
        assertEquals(dekanat.getGroups().get(1).getTitle(), tempStudent.getGroup().getTitle());
    }

    @Test
    public void excludeStudentsTest () throws Exception {
        dekanat.createStudents(this.studentFile);
        dekanat.createGroups(this.groupFile);
        dekanat.distributeStudentsByGroups(dekanat.getStudents(), dekanat.getGroups(), 10);
        for (Group g : dekanat.getGroups())
            dekanat.initHeadElection(g);
        Student tempStudent = dekanat.getStudents().get(0);
        String tempFio = tempStudent.getFio();
        dekanat.excludeStudents(tempStudent);
        for (Student s : dekanat.getStudents())
            assertNotEquals(tempFio, s.getFio());
    }

    @Test
    public void initHeadElectionTest () throws Exception {
        dekanat.createStudents(this.studentFile);
        dekanat.createGroups(this.groupFile);
        dekanat.distributeStudentsByGroups(dekanat.getStudents(), dekanat.getGroups(), 10);
        Group testGroup = dekanat.getGroups().get(0);
        testGroup.initHead(testGroup.getStudents().get(0));
        Student exHead = dekanat.getGroups().get(0).getHead();
        dekanat.initHeadElection(testGroup);
        assertNotEquals(exHead, testGroup.getHead());
    }

    @Test
    public void saveChangesToFileTest () throws Exception {
        String temp = new Scanner(new File(this.studentsPath)).useDelimiter("\\Z").next();
        dekanat.createStudents(this.studentFile);
        dekanat.createGroups(this.groupFile);
        dekanat.distributeStudentsByGroups(dekanat.getStudents(), dekanat.getGroups(), 10);
        for (Group g : dekanat.getGroups())
            dekanat.initHeadElection(g);
        dekanat.excludeStudents(dekanat.getStudents().get(0));
        dekanat.saveChangesToFile(this.studentFile);
        String newString = new Scanner(new File(this.studentsPath)).useDelimiter("\\Z").next();
        assertNotEquals(temp, newString);
    }

    @Test (expected = FileNotFoundException.class)
    public void saveChangesToFileExceptionTest () throws Exception {
        File file = new File("");
        dekanat.createStudents(this.studentFile);
        dekanat.saveChangesToFile(file);
    }
}