package com.expiated.dekanatdemo;

import org.junit.Before;
import org.junit.Test;

import java.io.FileNotFoundException;

import static org.junit.Assert.*;

public class GroupTest {

    private Group group;
    private Student student;

    @Before
    public void initTest () throws FileNotFoundException {
        this.student = new Student(1, "Lobachevsky");
        this.group = new Group();
    }

    @Test
    public void createGroupTest () {
        group.createGroup("some title");
        assertEquals("some title", group.getTitle());
    }

    @Test
    public void addStudentTest () {
        group.createGroup("some title");
        group.addStudent(student, group);
        assertEquals(student.getGroup().getTitle(), group.getTitle());
        assertEquals(student, group.getStudents().get(0));
    }

    @Test
    public void initHeadTest () {
        group.createGroup("some title");
        group.addStudent(student, group);
        group.initHead(student);
        assertEquals(student, group.getHead());
    }

    @Test
    public void averageMarkOfGroupTest () {
        group.createGroup("some title");
        for (int i = 0; i < 3; i++)
            group.addStudent(new Student(i, "fio" + i), group);
        for (Student s : group.getStudents())
            for (int i = 0; i < 3; i++)
                s.addMark(2 + i);
        assertEquals(3.0, group.averageMarkOfGroup(), 0);
    }

    @Test
    public void excludeStudentTest () {
        group.addStudent(student, group);
        int size = group.getStudents().size();
        group.excludeStudent(student);
        assertNotEquals(size, group.getStudents().size());
    }
}