package com.expiated.dekanatdemo;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class StudentTest {

    private Student student;

    @Before
    public void initTest () {
        this.student = new Student();
    }

    @Test
    public void createStudent() {
        this.student.createStudent(1, "Tesla");
        assertEquals(1, student.getId());
        assertEquals("Tesla", student.getFio());
    }

    @Test
    public void enrollStudent() {
        Group group = new Group();
        this.student.enrollStudent(group);
        assertEquals(group, student.getGroup());
    }

    @Test
    public void addMark() {
        this.student.addMark(3);
        assertEquals(3, (int)this.student.getMarks().get(0));
    }

    @Test
    public void averageMark() {
        for (int i = 0; i < 3; i++)
            this.student.addMark(2 + i);
        assertEquals(3, this.student.averageMark(this.student.getMarks()), 0);
    }
}