import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;

class Automata {

    Automata () {
        this.cash = 0;
        this.state = STATES.OFF;
    }

    public enum STATES {
        OFF, WAIT, CHECK,
        COOK, ACCEPT
    }

    private STATES state;

    private double cash;
    private static double bank;



    private String[] menu = {
            "americano", "russiano",
            "espresso", "cappucino",
            "latte", "mocha", "glace",
            "chocolate", "warm milk",
            "water"
    };

    private double[] prices = {
            40.0, 45.0, 25.0,
            50.0, 50.0, 50.0,
            55.0, 50.0, 30.0,
            10.0
    };

    public void setCash (double cash) {
        this.cash = cash;
    }

    public double getCash () {
        return this.cash;
    }

    public void setBank (double bank) {
        Automata.bank = bank;
    }

    public double getBank () {
        return Automata.bank;
    }

    public void setState(STATES state) {
        this.state = state;
    }

    public STATES getState() {
        return this.state;
    }

    public void on() {
        if (this.state == STATES.OFF)
            this.state = STATES.WAIT;
    }

    public void off() {
        if (this.state != STATES.OFF)
            this.state = STATES.OFF;
    }

    public void coin(double cash) {
        if (this.state == STATES.WAIT) {
            this.cash += cash;
            this.state = STATES.ACCEPT;
        }
    }

    public void printMenu() {
        if (this.state == STATES.WAIT || this.state == STATES.ACCEPT) {
            for (int i = 0; i < this.menu.length; i++)
                System.out.println("A cup of " + this.menu[i]
                        + " - " + this.prices[i] + " rubles.");
        }
    }

    public void printState() {
        if (this.state == STATES.WAIT)
            System.out.println("STATE: WAIT");
        else if (this.state == STATES.OFF)
            System.out.println("STATE: OFF");
        else if (this.state == STATES.CHECK)
            System.out.println("STATE: CHECK");
        else if (this.state == STATES.COOK)
            System.out.println("STATE: COOK");
        else if (this.state == STATES.ACCEPT)
            System.out.println("STATE: ACCEPT");
    }

    public double choice(String choice) {
        if (this.state == STATES.ACCEPT) {
            ArrayList<String> alMenu = new ArrayList<String>(Arrays.asList(this.menu));
            try {
                if (alMenu.contains(choice))
                    return this.prices[alMenu.indexOf(choice)];
            }
            catch (Throwable t) {
                System.out.println("There is no such a drink");
            }
        }
        return -1;
    }

    public void check(double price) {
        if (this.state == STATES.ACCEPT) {
            if (this.cash - price == 0) {
                Automata.bank += price;
                this.state = STATES.CHECK;
            }
            else {
                cancel();
                this.cash = 0;
                this.state = STATES.WAIT;
            }
        }
        else
            cancel();
    }

    public void cancel() {
        if (this.state != STATES.OFF && this.state != STATES.COOK){
            if (this.state == STATES.ACCEPT)
                this.cash = 0;
            if (this.state == STATES.CHECK) {
                Automata.bank -= this.cash;
                this.cash = 0;
            }
            this.state = STATES.WAIT;
        }
    }

    public void cook() {
        if (this.state == STATES.CHECK) {
            try {
                System.out.println("Wait..");
                Thread.sleep(5000);
                this.state = STATES.COOK;
                System.out.println("Taste your drink");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void finish() {
        if (this.state == STATES.COOK) {
            this.state = STATES.WAIT;
            this.cash = 0;
        }
    }

}