import junit.framework.TestCase;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class AutomataTest extends Assert {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    private Automata automate;

    @Before
    public void initTest () {
        this.automate = new Automata();
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
        automate.setState(Automata.STATES.WAIT);
        automate.setCash(0);
    }

    @After
    public void afterTest () {
        System.setOut(null);
        System.setErr(null);
        automate.setState(Automata.STATES.OFF);
        automate.setCash(0);
    }

    @Test
    public void onValidStateTest () {
        automate.setState(Automata.STATES.OFF);
        automate.on();
        assertEquals(Automata.STATES.WAIT, automate.getState());
    }

    @Test
    public void onInvalidStateTest () {
        automate.setState(Automata.STATES.CHECK);
        automate.on();
        assertNotEquals(Automata.STATES.WAIT, automate.getState());
    }

    @Test
    public void offValidStateTest () {
        automate.setState(Automata.STATES.WAIT);
        automate.off();
        assertEquals(Automata.STATES.OFF, automate.getState());
    }

    @Test
    public void offInvalidStateTest () {
        automate.setState(Automata.STATES.OFF);
        automate.off();
        assertEquals(Automata.STATES.OFF, automate.getState());
    }

    @Test
    public void coinValidStateTest () {
        automate.coin(25);
        assertEquals(Automata.STATES.ACCEPT, automate.getState());
    }

    @Test
    public void coinCashChangingTest () {
        automate.coin(25);
        assertEquals(25, automate.getCash(), 0);
    }

    @Test
    public void printMenuOutTest () {
        automate.printMenu();
        String str = outContent.toString();         //absolutely silly test which is made only
        assertEquals(str ,outContent.toString());   //to demonstrate ByteArrayOutputStream methods
    }

    @Test
    public void printStateOutTest () {
        automate.printState();
        assertEquals("STATE: WAIT\n" ,outContent.toString());
    }

    @Test
    public void choiceCorrectStringTest () {
        automate.setState(Automata.STATES.ACCEPT);
        assertNotEquals(-1, automate.choice("espresso"), 0);
    }

    @Test
    public void choiceUnsupportedStringTest () {
        automate.setState(Automata.STATES.ACCEPT);
        assertEquals(-1, automate.choice("sdsdkad"), 0);
    }

    @Test
    public void checkValidTest () {
        automate.setState(Automata.STATES.ACCEPT);
        automate.setCash(25);
        automate.check(25);
        assertEquals(Automata.STATES.CHECK, automate.getState());
    }

    @Test
    public void checkInalidTest () {
        automate.setCash(25);
        automate.check(25);
        assertNotEquals(Automata.STATES.CHECK, automate.getState());
    }

    @Test
    public void cancelCashTest () {
        automate.setState(Automata.STATES.ACCEPT);
        automate.cancel();
        assertEquals(0, automate.getCash(), 0);
    }

    @Test
    public void cancelRefundTest () {
        double temp = automate.getBank();
        automate.setState(Automata.STATES.ACCEPT);
        automate.setCash(25);
        automate.check(automate.choice("espresso"));
        automate.setState(Automata.STATES.CHECK);
        automate.cancel();
        assertEquals(temp, automate.getBank(), 0);
    }

    @Test
    public void cookValidTest () {
        automate.setState(Automata.STATES.CHECK);
        automate.cook();
        assertEquals(Automata.STATES.COOK, automate.getState());
    }

    @Test
    public void cookInvalidTest () {
        automate.setState(Automata.STATES.ACCEPT);
        automate.cook();
        assertEquals(Automata.STATES.ACCEPT, automate.getState());
    }

    @Test
    public void finishStateTest () {
        automate.setState(Automata.STATES.COOK);
        automate.finish();
        assertEquals(Automata.STATES.WAIT, automate.getState());
    }

    @Test
    public void finishCashTest () {
        automate.setState(Automata.STATES.COOK);
        automate.setCash(25);
        automate.finish();
        assertEquals(0, automate.getCash(), 0);
    }
}