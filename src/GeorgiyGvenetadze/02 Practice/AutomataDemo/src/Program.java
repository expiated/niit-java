public class Program {
    public static void main (String[] args) {
        Automata session = new Automata();
        double price;

        session.on();
        session.printMenu();
        session.coin(25);
        price = session.choice("espresso");
        session.check(price);
        session.cook();
        session.finish();
        session.off();
        session.printState();
    }
}
